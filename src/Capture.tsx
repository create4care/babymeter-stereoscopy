import * as React from 'react';
import * as RR from 'react-router-dom';

import Button from './Button.js';

import leftBaby from './assets/left.jpg';
import rightBaby from './assets/right.jpg';

const Capture = () => {
  const navigate = RR.useNavigate();
  const [transitioning, setTransitioning] = React.useState(false);
  const [capturing, setCapturing] = React.useState(false);

  const capture = () => {
    setTimeout(() => navigate('/measure'), 1000);
    setTimeout(() => setTransitioning(true), 500);

    setCapturing(true);
  };

  return (
    <div className='flex flex-col h-full'>
      <div className='flex flex-auto relative bg-neutral-900'>
        <div
          className='flex-auto bg-contain bg-center bg-no-repeat transition-all duration-500 ease-out'
          style={{ backgroundImage: `url(${leftBaby})` }}
        >
          <span className='absolute left-4 bottom-4 inline-flex items-center rounded-xl bg-black bg-opacity-75 px-2.5 py-0.5 text-sm font-medium text-gray-200 select-none'>
            Left view
          </span>
        </div>
        <div
          className={`${transitioning || 'flex-auto'} bg-contain bg-center bg-no-repeat transition-all duration-500`}
          style={{ backgroundImage: `url(${rightBaby})` }}
        >
          <span className={`${transitioning ? 'opacity-0' : 'opacity-100'} absolute right-4 bottom-4 inline-flex items-center rounded-xl bg-black bg-opacity-75 px-2.5 py-0.5 text-sm font-medium text-gray-200 transition-all duration-500 select-none`}>
            Right view
          </span>
        </div>
      </div>
      <div className='flex h-36 gap-4 items-center justify-center bg-neutral-800'>
        <Button onClick={capture} isLoading={capturing}>Capture</Button>
      </div>
    </div>
  );
};

export default Capture;
