import * as React from 'react';

export default ({ className, isLoading, children, ...rest }: React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
  className?: string,
  isLoading?: boolean,
  children?: React.ReactNode
}) => (
  <button
    type='button'
    className={`inline-flex items-center rounded-md bg-white px-7 h-12 text-lg font-semibold text-black whitespace-nowrap select-none transition-all focus:outline-none ${isLoading ? 'text-transparent bg-opacity-70 pointer-events-none ' : 'bg-opacity-100 hover:bg-opacity-80 active:bg-opacity-70 focus-visible:ring-2 focus-visible:ring-sky-500 focus-visible:ring-offset-2 focus-visible:ring-opacity-75 disabled:opacity-30 disabled:pointer-events-none'} ${className}`}
    {...rest}
  >
    <>
      {children}
      {isLoading &&
        <div className='absolute left-0 right-0 m-auto inline-block w-5 h-5 rounded-full border-2 border-b-transparent border-l-transparent border-r-black border-t-black animate-spin' />
      }
    </>
  </button>
);
