import * as RR from 'react-router-dom';
//import * as CV from 'opencv4nodejs';

import Measure from './Measure.js';
import Capture from './Capture.js';

const App = () => {
  return (
    <RR.BrowserRouter>
      <RR.Routes>
        <RR.Route path='/' element={<Capture />} />
        <RR.Route path='/measure' element={<Measure />} />
      </RR.Routes>
    </RR.BrowserRouter>
  );
};

export default App;
