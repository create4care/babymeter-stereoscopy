import * as React from 'react';
import * as RR from 'react-router-dom';
import { Dialog, Transition } from '@headlessui/react'
import Draggable, { DraggableData } from 'react-draggable';
import Xarrow, { Xwrapper } from 'react-xarrows';
import {
  MdInfoOutline,
  MdCameraAlt,
  MdBuild,
  MdDelete,
  MdSave,
  MdUndo,
  MdRedo,
  MdWarningAmber
} from 'react-icons/md';

import Button from './Button.js';

import leftBaby from './assets/left.jpg';
import rightBaby from './assets/right.jpg';

const enum View {
  LEFT, RIGHT, MAX
};

interface Point {
  x: number,
  y: number,
  undone: boolean
};

const Measure = () => {
  const [focus, setFocus] = React.useState(View.LEFT);
  const [focussing, setFocussing] = React.useState(false);
  const [dragging, setDragging] = React.useState(false);
  const [leftPoints, setLeftPoints] = React.useState<Point[]>([]);
  const [rightPoints, setRightPoints] = React.useState<Point[]>([]);
  const [magnifier, setMagnifier] = React.useState(false);
  const [[mW, mH], setmWH] = React.useState([0, 0]);
  const [[mX, mY], setmXY] = React.useState([0, 0]);
  const leftView = React.useRef<HTMLImageElement>(null);
  const rightView = React.useRef<HTMLImageElement>(null);
  let magnifyTimer: ReturnType<typeof setTimeout>;

  const [showNewMeasurement, setNewMeasurement] = React.useState(false);
  const cancelButtonRef = React.useRef(null);
  const navigate = RR.useNavigate();

  const focusPoints = () => {
    if (focus === View.LEFT)
      return leftPoints;
    else //else if (focus === View.RIGHT)
      return rightPoints;
  }

  const setFocusPoints = (value: React.SetStateAction<Point[]>) => {
    if (focus === View.LEFT)
      return setLeftPoints(value);
    else //else if (focus === View.RIGHT)
      return setRightPoints(value);
  }

  const maximizeView = () => {
    setFocussing(true);

    setFocus((focus + 1) % View.MAX);

    setTimeout(() => {
      setFocussing(false);
    }, 600);
  };

  const calcBgImgOffset = (target: HTMLDivElement | null) => {
    let offsetX = 0, offsetY = 0;

    if (!target)
      return { offsetX, offsetY };

    const bgSize = getComputedStyle(target, null).backgroundSize;

    /*
     * Determine the position of the click event relative to the picture
     * NOTE this assume we use background-position: center
     */
    const conW = target.clientWidth;
    const conH = target.clientHeight;

    // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
    // FIXME extract at runtime
    const imgW = 1280;
    const imgH = 720;

    const deltaW = conW - imgW;
    const deltaH = conH - imgH;

    if (deltaW > 0 && deltaH > 0) {
      if (bgSize == 'contain') {
        if (deltaH < deltaW * (imgH / imgW)) {
          offsetX = (conW - (imgH + deltaH) * (imgW / imgH)) / 2;
          offsetY = 0;
        } else {
          offsetX = 0;
          offsetY = (conH - (imgW + deltaW) * (imgH / imgW)) / 2;
        }
      } else if (bgSize == 'cover') {
        if (deltaH > deltaW * (imgH / imgW)) {
          offsetX = (conW - (imgH + deltaH) * (imgW / imgH)) / 2;
          offsetY = 0;
        } else {
          offsetX = 0;
          offsetY = (conH - (imgW + deltaW) * (imgH / imgW)) / 2;
        }
      }
    } else if ((bgSize == 'contain' && deltaW > 0) || (bgSize == 'cover' && deltaH > 0)) {
      offsetX = (conW - (imgH + deltaH) * (imgW / imgH)) / 2;
      offsetY = 0;
    } else if ((bgSize == 'contain' && deltaH > 0) || (bgSize == 'cover' && deltaW > 0)) {
      offsetX = 0;
      offsetY = (conH - (imgW + deltaW) * (imgH / imgW)) / 2;
    } else if (bgSize == 'contain') {
      if (Math.abs(deltaH) > Math.abs(deltaW) * (imgH / imgW)) {
        offsetX = (conW - (imgH + deltaH) * (imgW / imgH)) / 2;
        offsetY = 0;
      } else {
        offsetX = 0;
        offsetY = (conH - (imgW + deltaW) * (imgH / imgW)) / 2;
      }
    } else if (bgSize == 'cover') {
      if (Math.abs(deltaH) < Math.abs(deltaW) * (imgH / imgW)) {
        offsetX = (conW - (imgH + deltaH) * (imgW / imgH)) / 2;
        offsetY = 0;
      } else {
        offsetX = 0;
        offsetY = (conH - (imgW + deltaW) * (imgH / imgW)) / 2;
      }
    }

    return { offsetX, offsetY };
  };

  const elemToPxCoords = (x: number, y: number, w: number, h: number, target: HTMLDivElement) => {
    const { offsetX, offsetY } = calcBgImgOffset(target);

    return {
      pX: Math.round((w * (x - offsetX)) / (target.clientWidth - 2 * offsetX)),
      pY: Math.round((h * (y - offsetY)) / (target.clientHeight - 2 * offsetY))
    };
  };

  const pxToElemCoords = (x: number, y: number, w: number, h: number, target: HTMLDivElement) => {
    const { offsetX, offsetY } = calcBgImgOffset(target);

    return {
      pX: Math.round(((offsetX * w) - (2 * offsetX * x) + (target.clientWidth * x)) / w),
      pY: Math.round(((offsetY * h) - (2 * offsetY * y) + (target.clientHeight * y)) / h)
    };
  };

  const addPoint = (e: any) => {
    const points = focusPoints().filter(e => !e.undone);

    clearTimeout(magnifyTimer);

    if (!e.target || !e.target.classList.contains('view'))
      return;

    // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
    // FIXME extract at runtime
    const imgW = 1280;
    const imgH = 720;

    /* Convert these coordinates to pixel coordinates */
    const { pX, pY } = elemToPxCoords(e.pageX, e.pageY, imgW, imgH, e.target);

    /* Check if we're within image bounds */
    if (pX < 0 || pX > imgW || pY < 0 || pY > imgH)
      return;

    points.push({
      x: pX,
      y: pY,
      undone: false
    });

    setFocusPoints(points);
    setmXY([e.pageX, e.pageY]);
    setMagnifier(true);

    /* TODO mouse down on magnifying glass */
  };

  const mouseUpHandler = () => {
    magnifyTimer = setTimeout(() => {
      setMagnifier(false);
    }, 500);
  };

  const onWindowResize = () => {
    const target = focus === View.LEFT ? leftView.current : rightView.current;

    if (!target || !target.parentElement)
      return;

    const { offsetX, offsetY } = calcBgImgOffset(target);
    const { width, height } = target.parentElement.getBoundingClientRect();

    setmWH([width - 2 * offsetX, height - 2 * offsetY]);
  };

  const dragHandler = (e: any, data: DraggableData) => {
    clearTimeout(magnifyTimer);

    if (!e.target || !e.target.parentNode)
      return;

    setmXY([data.x, data.y]);
  };

  const movePoint = (e: any, data: DraggableData) => {
    const points = [...focusPoints()];

    setDragging(false);

    if (!e.target || !e.target.parentElement)
      return;

    // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
    // FIXME extract at runtime
    const imgW = 1280;
    const imgH = 720;

    /* Convert these coordinates to pixel coordinates */
    const { pX, pY } = elemToPxCoords(data.x, data.y, imgW, imgH, e.target.parentElement);

    /* Check if we're within image bounds */
    if (pX < 0 || pX > imgW || pY < 0 || pY > imgH)
      return;

    points[e.target.id.split('-').pop()] = {
      x: pX,
      y: pY,
      undone: false
    };

    setFocusPoints(points);
  };

  const numPoints = (pnts: Point[]) => pnts.filter(e => !e.undone).length;

  /* Check if the current view has any points (excluding undone points) */
  const hasPoints = (undone: boolean) => {
    if (focusPoints().length == 0)
      return false;

    if (undone && focusPoints().every(e => e.undone))
      return false;

    return true;
  };

  /* Check if the current view has any points to be redone */
  const hasRedoPoints = () => {
    if (focusPoints().length == 0)
      return false;

    if (focusPoints().some(e => e.undone))
      return true;
  };

  const undoPoint = () => {
    let points = [...focusPoints()];

    const point = points.reverse().find(e => !e.undone);
    if (!point)
      return;
    point.undone = true;
    setFocusPoints(points.reverse());
  };

  const redoPoint = () => {
    const points = [...focusPoints()];

    const point = points.find(e => e.undone);
    if (!point)
      return;
    point.undone = false;
    setFocusPoints(points);
  };

  const clearPoints = () => setFocusPoints([]);

  /* TODO update during resizing of preview */
  const renderPoints = (view: View) => {
    let elems = [];
    let index = 0;

    const viewobj = (view === View.LEFT) ? leftView : rightView;
    if (!viewobj || !viewobj.current)
      return;

    // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
    // FIXME extract at runtime
    const imgW = 1280;
    const imgH = 720;
    //const bgSize = getComputedStyle(viewobj.current, null).backgroundSize;

    for (const point of (view === View.LEFT) ? leftPoints : rightPoints) {
      if (point.undone)
        continue;

      const { pX, pY } = pxToElemCoords(point.x, point.y, imgW, imgH, viewobj.current);

      elems.push(
        <Draggable
          key={'point-' + index}
          bounds='parent'
          onStart={(e: any, data: DraggableData) => {
            dragHandler(e, data);
            setDragging(true);
            setMagnifier(true);
          }}
          onDrag={dragHandler}
          onStop={movePoint}
          position={{ x: pX, y: pY }}
        >
          <div
            id={`${view}-point-${index}`}
            className={`absolute bg-[red] rounded-full cursor-grab z-40 transition-opacity duration-200 ${focussing ? 'opacity-0' : 'opacity-100'} ${focus === view ? 'w-5 h-5 -mt-[10px] -ml-[10px]' : 'w-1 h-1 -mt-[2px] -ml-[2px]'}`}
            onClick={undefined}
          />
        </Draggable>
      );

      const labelDraw = (index: number) => {
          if (index >= numPoints(leftPoints))
            return <></>;
          return (
            <div
              className={`bg-black text-gray-200 select-none px-2 rounded-md text-sm transition-opacity duration-200 ${focus === view ? 'opacity-100' : 'opacity-0'}`}
            >
              13 cm
            </div>
          );
      };

      if (index > 0) {
        elems.push(
          <Xarrow
            key={'line-' + index}
            start={`${view}-point-${index - 1}`}
            end={`${view}-point-${index}`}
            startAnchor='middle'
            endAnchor='middle'
            path='straight'
            strokeWidth={focus === view ? 2 : 0.3}
            divContainerStyle={{
              opacity: focussing ? '0' : '1',
              transition: 'opacity 0.2s'
            }}
            lineColor='black'
            showHead={false}
            labels={labelDraw(index)}
          />
        );
      }

      index++;
    }

    return elems;
  };

  const magnifierRadius = 125;
  const magnifierZoom = 1.5; // TODO move to settings

  const { offsetX: leftOffsetX, offsetY: leftOffsetY } = calcBgImgOffset(leftView.current);
  const { offsetX: rightOffsetX, offsetY: rightOffsetY } = calcBgImgOffset(rightView.current);
  const focusOffsetX = focus === View.LEFT ? leftOffsetX : rightOffsetX;
  const focusOffsetY = focus === View.LEFT ? leftOffsetY : rightOffsetY;

  React.useEffect(() => {
    window.addEventListener('resize', onWindowResize, false);
    onWindowResize();
  }, []);

  return (
    <div className='flex flex-col h-full'>
      <div className='relative flex-auto w-full bg-neutral-900'>
        <div className='absolute inline-flex gap-2 items-center p-3 px-6 top-4 left-1/2 -translate-x-1/2 z-40 text-md bg-black text-white select-none rounded-full bg-opacity-75'>
          <MdInfoOutline />
          {
            // FIXME only count !undone points
            numPoints(leftPoints) > 0 && numPoints(rightPoints) > 0 && numPoints(leftPoints) == numPoints(rightPoints) ? (
              `Total length: ${13 * (numPoints(leftPoints) - 1)} cm`
            ) : (
              'Place corresponding points on both images'
            )
          }
        </div>
        <div
          ref={leftView}
          className={`view absolute bg-center bg-no-repeat duration-500 ${focus === View.LEFT ? 'visible opacity-100 z-10 left-0 bottom-0 w-full h-full rounded-none bg-contain shadow-none' : `${dragging ? 'opacity-0' : 'opacity-100'} z-20 left-3 bottom-3 w-60 h-40 rounded-xl bg-cover duration-500 shadow-lg`}`}
          style={{ backgroundImage: `url(${leftBaby})` }}
          onMouseDown={(e: any) => focus === View.LEFT ? addPoint(e) : maximizeView()}
          onMouseUp={mouseUpHandler}
          onTouchEnd={mouseUpHandler}
        >
          <Xwrapper>
            {renderPoints(View.LEFT)}
          </Xwrapper>
          <span className='absolute left-4 bottom-4 inline-flex items-center rounded-xl bg-black bg-opacity-75 px-2.5 py-0.5 text-sm font-medium text-gray-200 select-none'>
            Left view
          </span>
        </div>
        <div
          ref={rightView}
          className={`view absolute bg-center bg-no-repeat duration-500 ${focus === View.RIGHT ? 'visible opacity-100 z-10 right-0 bottom-0 w-full h-full rounded-none bg-contain shadow-none' : `${dragging ? 'opacity-0' : 'opacity-100'} z-20 right-8 bottom-8 w-60 h-40 rounded-xl bg-cover duration-500 shadow-lg`}`}
          style={{ backgroundImage: `url(${rightBaby})` }}
          onMouseDown={(e: any) => focus === View.RIGHT ? addPoint(e) : maximizeView()}
          onMouseUp={mouseUpHandler}
          onTouchEnd={mouseUpHandler}
        >
          {renderPoints(View.RIGHT)}
          <span className='absolute right-4 bottom-4 inline-flex items-center rounded-xl bg-black bg-opacity-75 px-2.5 py-0.5 text-sm font-medium text-gray-200 select-none'>
            Right view
          </span>
        </div>
        <div
          className={`absolute z-40 pointer-events-none transition-opacity duration-200 ease-in-out border border-gray-200 rounded-full bg-neutral-900 bg-no-repeat ${magnifier ? 'visible opacity-100' : 'hidden opacity-0'}`}
          style={{
            top: (mY < (magnifierRadius / 2 + 80) ? ( mY - magnifierRadius / 2 + 80) : ( mY - magnifierRadius / 2 - 80)) + 'px',
            left: mX - magnifierRadius / 2 + 'px',
            width: magnifierRadius + 'px',
            height: magnifierRadius + 'px',
            backgroundImage: `url(${focus === View.LEFT ? leftBaby : rightBaby})`,
            backgroundSize: `${mW * magnifierZoom}px ${mH * magnifierZoom}px`,
            backgroundPositionX: `${(-mX + focusOffsetX) * magnifierZoom + magnifierRadius / 2}px`,
            backgroundPositionY: `${(-mY + focusOffsetY) * magnifierZoom + magnifierRadius / 2}px`
          }}
        >
          <div
            className='absolute -mt-[1px] h-[3px] bg-black border-t border-b border-gray-200 opacity-50'
            style={{
              top: magnifierRadius / 2,
              left: magnifierRadius / 4,
              width: magnifierRadius / 2
            }}
          />
          <div
            className='absolute -ml-[1px] w-[3px] bg-black border-l border-r border-gray-200 opacity-50'
            style={{
              top: magnifierRadius / 4,
              left: magnifierRadius / 2,
              height: magnifierRadius / 2
            }}
          />
        </div>
      </div>
      <div className='h-36 bg-neutral-800 p-16'>
        <div className='flex h-full m-auto gap-4 items-center justify-center max-w-6xl'>
          <span className='isolate inline-flex rounded-md'>
            <Button className='rounded-r-none' disabled={!hasPoints(true)} onClick={undoPoint}>
              <MdUndo className='-ml-0.5 mr-2 h-4 w-4' />
              Undo
            </Button>
            <Button className='rounded-l-none' disabled={!hasRedoPoints()} onClick={redoPoint}>
              <MdRedo className='-ml-0.5 mr-2 h-4 w-4' />
              Redo
            </Button>
          </span>
          <Button disabled={!hasPoints(true)} onClick={clearPoints}>
            <MdDelete className='-ml-0.5 mr-2 h-4 w-4' />
            Clear points
          </Button>
          <div className='flex-auto self-stretch' />
          <Button aria-label='Settings'>
            <MdBuild className='-ml-0.5 h-4 w-4' />
          </Button>
          <Button>
            <MdSave className='-ml-0.5 mr-2 h-4 w-4' />
            Save
          </Button>
          <Button onClick={() => setNewMeasurement(true)}>
            <MdCameraAlt className='-ml-0.5 mr-2 h-4 w-4' />
            New measurement
            <Transition.Root show={showNewMeasurement} as={React.Fragment}>
              <Dialog as="div" className="relative z-50" initialFocus={cancelButtonRef} onClose={setNewMeasurement}>
                <Transition.Child
                  as={React.Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-0 z-50 overflow-y-auto">
                  <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                    <Transition.Child
                      as={React.Fragment}
                      enter="ease-out duration-300"
                      enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                      enterTo="opacity-100 translate-y-0 sm:scale-100"
                      leave="ease-in duration-200"
                      leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                      leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                      <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                        <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                          <div className="sm:flex sm:items-start">
                            <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                              <MdWarningAmber className="h-6 w-6 text-red-600" aria-hidden="true" />
                            </div>
                            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                                New measurement
                              </Dialog.Title>
                              <div className="mt-2">
                                <p className="text-sm text-gray-500">
                                  Are you sure you want to discard this measurement and take a new measurement?
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                          <button
                            type="button"
                            className="inline-flex w-full justify-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={() => navigate('/')}
                          >
                            Discard
                          </button>
                          <button
                            type="button"
                            className="mt-3 inline-flex w-full justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-base font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-sky-500 focus:ring-offset-2 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                            ref={cancelButtonRef}
                            onClick={() => setNewMeasurement(false)}
                          >
                            Cancel
                          </button>
                        </div>
                      </Dialog.Panel>
                    </Transition.Child>
                  </div>
                </div>
              </Dialog>
            </Transition.Root>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Measure;
