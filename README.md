# Babymeter 4

New UI concept for Babymeter. Install dependencies using:
```
$ yarn install
```

Then launch electron frontend with:
```
$ yarn dev
```
